from django.urls import path, include
from .views import blog, project_detail, add_contact

urlpatterns = [
    path('', blog, name='blog'),
    path('post/<int:pk>/', project_detail, name='project_detail'),
    path('blog/', add_contact, name='add_contact'),
]
