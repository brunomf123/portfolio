from django.shortcuts import render, get_object_or_404
from .models import Project, Contact


def blog(request):
    projects = Project.objects.filter(approved=True)

    data = {
        'projects': projects,
    }

    return render(request, 'index.html', data)


def project_detail(request, pk):
    project = get_object_or_404(Project, pk=pk)
    return render(request, 'project_detail.html', {'project': project})


def add_contact(request):
    form = request.POST

    contact = Contact()
    contact.author = form['author']
    contact.email = form['email']
    contact.title = form['title']
    contact.content = form['content']

    contact.save()

    contact.send_email()

    return render(request, 'sucess.html', {'contact': contact})
