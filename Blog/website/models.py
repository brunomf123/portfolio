from django.db import models
from email.mime.text import MIMEText as text
import smtplib


class Contact(models.Model):
    author = models.CharField(max_length=100)
    email = models.EmailField()
    title = models.CharField(max_length=100)
    content = models.TextField()

    def __str__(self):
        return self.author

    def send_email(self):
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.login("brunomf123@gmail.com", "zzkrsyodsyhcbcwq")

        msg = 'Título: {}\nAutor: {}\nE-mail: {}\n\n{}'.format(self.title, self.author, self.email, self.content)

        message = text(msg)

        message['From'] = 'brunomf123@gmail.com'
        message['To'] = 'brunomf123@gmail.com'
        message['Subject'] = "Nova mensagem no Porfólio!"

        server.sendmail(
            "brunomf123@gmail.com",
            "brunomf123@gmail.com",
            message.as_string())

        server.quit()


class Categories(models.TextChoices):
    TCC = 'TCC', 'TCC'
    FRL = 'FRL', 'Freelance'
    FED = 'FED', 'Fins Educativos'


class Project(models.Model):
    title = models.CharField(max_length=100)
    sub_title = models.CharField(max_length=200)
    content = models.TextField()
    category = models.CharField(
        max_length=3,
        choices=Categories.choices,
        default=Categories.FED
    )
    approved = models.BooleanField(default=True)
    image = models.ImageField(upload_to='projects', null=True, blank=True)

    def __str__(self):
        return self.title

    def get_category_label(self):
        return self.get_category_display()
