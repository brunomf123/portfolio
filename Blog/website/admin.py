from django.contrib import admin
from .models import Project, Contact


class ProjectAdmin(admin.ModelAdmin):
    list_display = ['title', 'sub_title', 'category', 'approved']
    search_fields = ['title', 'sub_title', 'category', 'approved']

    def get_queryset(self, request):
        return Project.objects.filter(approved=True)


class ContactAdmin(admin.ModelAdmin):
    list_display = ['author', 'email', 'title']
    search_fields = ['author', 'email', 'title']


admin.site.register(Project, ProjectAdmin)
admin.site.register(Contact, ContactAdmin)
